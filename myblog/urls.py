from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import RedirectView
# import blog

urlpatterns = [
    # Examples:
    # url(r'^$', 'myblog.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'blog.views.index', name='home'),
    url(r'^post/(?P<post_url>.*)/+$', 'blog.views.post', name='post'),
    url(r'^about/+$','blog.views.about',name='about'),
    url(r'^contact/+$','blog.views.contact',name='contact'),
    url(r'^gallery/+$','blog.views.gallery',name='gallery'),
    url(r'^archive/+$','blog.views.archive',name='archive'),
    url(r'^archive/(?P<archive_index>\d*)/+$','blog.views.archive',name='archive'),
    url(r'^favicon\.ico$', RedirectView.as_view(url='static/images/favicon.ico')),
    
    ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
    + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) 
