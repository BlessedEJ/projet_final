from django.shortcuts import render_to_response ,get_object_or_404
from django.http import Http404
from django.http import HttpResponse
from blog.models import article
from django.conf import settings

site=settings.SITE

# Create your views here.
def index(request):
    a=article.objects.order_by('-id')[:site['articlesPerPage']]
    count=a.count()
    posts=[]
    for post in a:
        posts.append({'title':post.title,
                      'subtitle':post.subtitle,
                      'author':post.author.name,
                      'text':post.text,
                      'posturl':post.posturl,
                      'date':post.date, })
    
    paginator = {'posts':posts,
                 'total_pages':count,
                 'previous_page':False,
                 'next_page':False,
                 'previous_page_path':'0',
                 'next_page_path':'2',
                 }
    if article.objects.count()>count:
        paginator['next_page']=True
    return render_to_response('blog/index.html',{'site':site,'paginator':paginator})

def archive(requests,archive_index='1'):
    #Nombre de page
#     return HttpResponse(archive_index)
    articleCount = article.objects.count()
    articlePerpage=site['articlesPerPage']
    i=articleCount/articlePerpage
    pageCount =  (i+1) if(articleCount>i*articlePerpage) else i
    try:archive_index=int(archive_index)
    except: raise Http404
    if archive_index>pageCount:
        raise Http404
    
    a=article.objects.order_by('-id')[(archive_index-1)*10:archive_index+articlePerpage]
    count=a.count()
    posts=[]
    for post in a:
        posts.append({'title':post.title,
                      'subtitle':post.subtitle,
                      'author':post.author.name,
                      'text':post.text,
                      'posturl':post.posturl,
                      'date':post.date, })
    paginator = {'posts':posts,
                 'total_posts':count,
                 'pageCount':range(1,pageCount+1),
                 'currentPage':archive_index,
                 'previous_page':False,
                 'previous_page_path':'',
                 'next_page':False,
                 'next_page_path':'',
                 }
    if archive_index>1:
        paginator['previous_page']=True
        paginator['previous_page_path']=archive_index-1
    if archive_index<pageCount:
        paginator['next_page']=True
        paginator['next_page_path']=archive_index+1
    if pageCount>9:
        if archive_index>4:
            if archive_index<pageCount-4:
                paginator['pageCount']=range(archive_index-4,archive_index+5)
            else:
                paginator['pageCount']=range(pageCount-9,pageCount+1)
                
                
    page={'title':'Archive','headerimg':'','description':'Les differents articles'}
    return render_to_response('blog/archive.html',{'site':site,'paginator':paginator,'page':page})    
            

def post(request, post_url):
    try:
        page = article.objects.get(posturl=post_url)
    except:
        if type(post_url)!=int:
            raise Http404
        page = get_object_or_404(article,id=post_url)
    localvars = {'site':site,
                  'page':{'title':page.title,
                          'url':page.posturl,
                          'subtitle':page.subtitle,
                          'author':page.author.name,
                          'date':page.date,
                          'lastEdit':page.lastEdit,
                          'text':page.text,
                          }
          }
    try:
        p = article.objects.get(id=page.id - 1)
        localvars['page']['previous'] = {'posturl':p.posturl, 'title':p.title}
    except:
        pass
    try:
        n = article.objects.get(id=page.id + 1)
        localvars['page']['next'] = {'posturl':n.posturl, 'title':n.title}
    except:
        pass
    
    return render_to_response('blog/post.html', localvars)

def about(requests):
    page={
    'title': 'Apropos',
    'description': "Le blog de l'ESIH.",
    }
    return render_to_response('blog/about.html',{'site':site,'page':page})

def contact(requests):
    page={
    'title': 'Contact',
    'description': 'Contactez nous par Email.',
    }
    return render_to_response('blog/contact.html',{'site':site,'page':page})
def gallery(requests):
    return HttpResponse('<h1>Gallery</h1>')

def feed(requests):
    a=article.objects.order_by('-id')
    posts=[]
    for post in a:
        posts.append({'title':post.title,
                      'author':post.author.name,
                      'text':post.text,
                      'posturl':post.posturl,
                      'date':post.date,
                      })
    return render_to_response('', {'site':site,'posts':posts})


# Detail generate


