'''
Created on 2015-4-10

@author: evens
'''
from django.template import Library

register=Library()

@register.filter
def date_to_rfc822(date):
    return date.strftime(r'%a, %d %b %Y %H:%M:%S')
# <pubDate>{{ now|date:"D, d M Y H:i:s O" }}</pubDate>

@register.filter
def prepend(s,arg):
    return arg+s

@register.filter
def lastedit(date):
    from datetime import datetime
    from django.utils.timezone import utc
    now = datetime.utcnow().replace(tzinfo=utc)
    time=now-date.replace(tzinfo=utc)
    days=int(time.days)
    seconds=int(time.seconds)
    if days>3:
        return time.strftime(r'%F -%d, %Y')
    elif days>0:
        return "%d jours et il y a %d heures "%(days,seconds/3600)
    elif seconds/3600>0:
        return "%d Heure et %d Minutes"%(seconds/3600,seconds%3600/60)
    elif seconds/60>1:
        return "il y a %d minutes "%(seconds/60,)
    else:
        return "maintenant"
    
@register.filter
def replace(s,arg1,arg2):
    return s.replace(arg1,arg2)

