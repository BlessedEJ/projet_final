from django.db import models
from datetime import datetime

def generatePostUrl():
    return datetime.now().strftime('%Y%M%d%S')
    

# Create your models here.
class Author(models.Model):
    name=models.CharField(max_length=64)
    sex=models.CharField(max_length=2,choices=(('MA','male'),('FE','female')),default='MA')
    birthday=models.DateField()
    avatar=models.ImageField(upload_to='./avatars',null=True,blank=True)
    sign=models.CharField(max_length=128,null=True,blank=True)
    email=models.EmailField(null=True)
    def __unicode__(self):
        return self.name
    
class article(models.Model):
    author=models.ForeignKey(Author)
    title=models.CharField(max_length=64)
    posturl=models.CharField(max_length=32,null=True,blank=True,default=generatePostUrl)
    subtitle=models.CharField(max_length=128,null=True,blank=True)
    titleImage=models.ImageField(upload_to='./up',blank=True)
#     date=models.DateTimeField(auto_now_add=True,editable=True)
    date=models.DateTimeField(default=datetime.now,blank=True)
    lastEdit=models.DateTimeField(auto_now=True,editable=True)
    text=models.TextField()
    def __unicode__(self):
        return self.title