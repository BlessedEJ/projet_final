$(function() {

	$('.back-to-top').hide();
	$('.back-to-top').css('visibility','visible')

	$(window).scroll(function() {
		if ($(this).scrollTop() > 150) {
			$('.back-to-top').fadeIn(500);
		} else {
			$('.back-to-top').fadeOut(500);
		}
	});


	$('.back-to-top').click(function(event) {
		event.preventDefault();
		$('html, body').animate({
			scrollTop : 0
		}, 500);
	})
})